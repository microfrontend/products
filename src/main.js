import Vue from 'vue'
import VueCustomElement from 'vue-custom-element'
import App from './App'
import store from './store'

Vue.config.productionTip = false
Vue.use(VueCustomElement)

App.store = store

// Vue.customElement('ssk-product', App)
Vue.customElement('ssk-product', App)
