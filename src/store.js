import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    products: ['Rice', 'Beans', 'Fries', 'Steak'],
    carts: []
  },
  mutations: {
    addToCart (state, product) {
      const index = state.carts.findIndex(item => item.product === product)
      if (index !== -1) {
        state.carts[index].count++
      } else {
        state.carts.push({ product, count: 1 })
      }
    }
  },
  actions: {
    addToCart ({ commit }, product) {
      commit('addToCart', product)
    }
  }
})
